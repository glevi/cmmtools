Examples of using CMMSystem to do MD
====================================

The more advanced ones are basically runscripts, so not very pedagogical, but performing a lot of tasks :) 


`example.py`
-----------
Simplest possible run. 

`agptpop_es_constrainedopt_conandres_noNPT.py`
----------------------------------------------
Running solvent sampling simulations for all ES ChelpG charge sets made by scanning the vdw radii of Ag and Pt in AgPtPOP.  

The ES structure is made by updating the original positions in the system with an xyz file containing positions from the first
run of x-ray structural fitting of the metals, and DFT-relaxed ligands. 


`meptpop_kruppa.py`
-------------------
Sample shells of all Kruppa structures. Look in here for an idea for how to reindex molecules based on closest distance to atoms
of the same element. Tests of this are also visualized in `/data/md/tlptpop/TlPtPOP_shells.ipynb`