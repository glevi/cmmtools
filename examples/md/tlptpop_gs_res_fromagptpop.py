import rmsd
import numpy as np
from os.path import isdir
from simtk.unit import kilojoule_per_mole, angstroms
import simtk.unit as u
from cmm.md.cmmsystems import CMMSystem
from cmm.tools.parsers import read_chargefile
from parmed.openmm.reporters import RestartReporter
from ase.io import read
from simtk.openmm.openmm import HarmonicAngleForce 

''' First TlPtPOP Solvation Shell sampling run: 

    1. Start from the same good AgPtPOP trajectory as with the Ag runs
    2. Reset the position to the Tl geom opt from the Gaussian run
        The atomic sequence is the same, Ag serves as Tl.
    3. Set the charges
    4. Change the 'Ag' LJ parameters to those of Tl from 10.1021/ct500918t:
        rmin/2: 1.870 Ang, eps:0.27244486 kcal/mol

'''


with open('../../data/md/tlptpop/tlptpop_gs_pcm_chelpg_charges_from_opt1.dat', 'r') as f:
    lines = f.readlines()



equilname = f'tlptpop_gs_res_fromtlptpop_equi'
produname = f'tlptpop_gs_res_fromtlptpop_prod'

fpath = f'/work1/asod/openMM/data/TlPtPOP/'

# New charges:
charges = np.array([float(line.split()[2]) for line in lines if len(line) > 1])
print(f'Sum of charges: {sum(charges)}')
idx = list(range(len(charges)))

#### init - already pushed away ions when we start
sys = CMMSystem(equilname, fpath=fpath)

sys.system_from_prmtop('../../data/md/agptpop/agptpop_solv_custom.prmtop')

sys.load_rst7('/zhome/c7/a/69784/openMM/data/AgPtPOP/STARTFROMTHIS/agptpop_nvt_pushaway_Pt1.50_Ag2.25.rst7')

# set new charges
sys.adjust_solute_charges(idx, charges)

# Set the LJ parameter of the 'Ag' atom to the ones of Tl, effectively making the
# atom Tl
rminhlf =  1.870 
sig_tl = rminhlf * 2**(5/6)
eps_tl = 0.27244486 * u.kilocalorie_per_mole
ljdict = {'AG':(eps_tl, sig_tl * u.angstroms)}
sys.adjust_lj([0], ljdict)

# Set positions to TlPtPOP
ns_idx, syms = sys.get_nonsolvent_indices()
solu_idx = [i for i, sym in enumerate(syms) if sym != 'Na+']
pos = sys.positions.value_in_unit(angstroms)
pos = np.array([[p.x, p.y, p.z] for p in pos])
old_pos = pos.copy()[solu_idx]
new_atoms = read('../../data/md/tlptpop/gs_run1_last_step.xyz')
pos_a = new_atoms.get_positions()  # align to gs pos in box first
pos_t = pos[solu_idx]
pos_a += -rmsd.centroid(pos_a) + rmsd.centroid(pos_t)  # translate
U = rmsd.kabsch(pos_a, pos_t)  # rotate
pos_a = np.dot(pos_a, U)
pos[solu_idx] = pos_a  # overwrite old gs pos with aligned new pos
sys.positions = pos * angstroms

# constrain metal distances
tlpt = np.linalg.norm(pos[0] - pos[2]) * angstroms
sys.add_constraint(0, 2, tlpt)
for i in range(8622):
    c = sys.system.getConstraintParameters(i) 
    if c[0] == 0:
        print(i, c)
        break
ptpt = np.linalg.norm(pos[1] - pos[2]) * angstroms
sys.add_constraint(1, 2, ptpt)

# RESTRAIN ATOMS
sys.restrain_atoms(ns_idx, 2000)

# and integrator
sys.set_integrator(timestep=0.5)  
#sys.integrator.setConstraintTolerance(1e-12)

# init sim
sys.init_simulation(platform='CUDA', double=False)

# reporters
sys.standard_reporters()

# restart reporter
restrt = RestartReporter(fpath + f'{equilname}.rst7', 1000) 
sys.add_reporter(restrt)

# runnnn
sys.run(time_in_ps=200)

## Production
sys = CMMSystem(produname, fpath=fpath)
sys.system_from_prmtop('../../data/md/agptpop/agptpop_solv_custom.prmtop')
sys.load_rst7(fpath + f'{equilname}.rst7')

# constrain metal distances
tlpt = np.linalg.norm(pos[0] - pos[2]) * angstroms
sys.add_constraint(0, 2, tlpt)
ptpt = np.linalg.norm(pos[1] - pos[2]) * angstroms
sys.add_constraint(1, 2, ptpt)

# RESTRAIN ATOMS
sys.restrain_atoms(ns_idx, 2000)

# set new charges
sys.adjust_solute_charges(idx, charges)

# and new LJ params 
rminhlf =  1.870 
sig_tl = rminhlf * 2**(5/6)
eps_tl = 0.27244486 * u.kilocalorie_per_mole
ljdict = {'AG':(eps_tl, sig_tl * u.angstroms)}
sys.adjust_lj([0], ljdict)

# and integrator
sys.set_integrator(timestep=2.0)  

# init sim
sys.init_simulation(platform='CUDA', double=False)

# reporters
sys.standard_reporters(step=250)

# restart reporter
restrt = RestartReporter(fpath + f'{produname}.rst7', 1000) 
sys.add_reporter(restrt)

# runnnn
sys.run(time_in_ps=10000)
