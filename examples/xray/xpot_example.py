import numpy as np
import matplotlib.pyplot as plt
from ase import Atoms, units
from ase.io import Trajectory
from ase.constraints import Hookean
from ase.md.verlet import VelocityVerlet
from ase.md.nvtberendsen import NVTBerendsen
from cmm.xray.xpot import DebyeCalc
from cmm.xray.debye import Debye
from cmm.tools.helpercalcs import DoubleCalc, SimpleBond

#### First make the 'target' dynamics
dt = 0.2

# Make diatomic Atoms object 
atoms = Atoms('CC', positions=np.array([[-1, 0, 0], [1, 0, 0]]))

# Set simple Hookean calculator with x0 = 1.5 Ang.
k = 500 * units.kJ / units.mol
atoms.calc = SimpleBond([[0, 1]],  # list of atom-indices to put bonds
                         [k],  # list of spring constants
                         [1.5])  # list of eq. distances. 

# Use Berendsen dynamics to make damped dynamics 
dyn = NVTBerendsen(atoms, timestep=dt * units.fs, temperature=0.00001 * units.kB, taut=50 * units.fs,
                   trajectory='target.traj', logfile='target.log')

# Run for 400 fs
print('Making target trajectory')
dyn.run(2000)


#### Get "uncorrected" trajectory, e.g. poor spring constant, and no damping:

atoms = Atoms('CC', positions=np.array([[-1, 0, 0], [1, 0, 0]]))
atoms.calc =  SimpleBond([[0, 1]], [1.5 * k], [1.5])
# Use verlet so there is no damping
dyn = VelocityVerlet(atoms, timestep=dt * units.fs,
                     trajectory='poor.traj', logfile='poor.log')

print('Making \'poor\' trajectory')
dyn.run(2000)


#### Do the Debye-driven dynamics on the poor potential to get forces on the atoms
#### This version actually moves the atoms with the forces. For the plot 
#### we want to see the forces produced from the scattering difference between 
#### the poor trajectory and the target.
#atoms = Atoms('CC', positions=np.array([[-1, 0, 0], [1, 0, 0]]))

# Use both the Debye and the Hookean calculator:
#atoms.calc = DoubleCalc(DebyeCalc(Debye(), None, update=correct, k=1e-2), 
#                                 SimpleBond([[0, 1]], [1.5 * k], [1.5]))


#dyn = VelocityVerlet(atoms, timestep=dt * units.fs,
#                     trajectory='corrected.traj', logfile='corrected.log')
#
#dyn.run(2000)


#### Get forces
target = Trajectory('target.traj', 'r')
poor = Trajectory('poor.traj', 'r')
db = Debye()

fsteps = int(2000 / 40 + 1)
f = np.zeros((fsteps, 6))
print('Calculating WAXS forces')
ctf = 0
for a, atoms in enumerate(poor):
    if a % 40 == 0:
        s_exp = db.debye(target[a])
        atoms.calc = DebyeCalc(db, s_exp, k=1e-2)
        f[ctf] = atoms.get_forces().flatten()
        ctf += 1
        if ctf % 5 == 0:
            print(f'{ctf * 2:3d} % Done')
    
f = f.reshape((fsteps, 2, 3))


#### Make the plot
d_target = np.array([atoms.get_distance(0, 1) for atoms in target])
t_target = np.linspace(0, dt * len(d_target), len(d_target))

d_poor = np.array([atoms.get_distance(0, 1) for atoms in poor])
t_poor = np.linspace(0, dt * len(d_poor), len(d_poor))


ncs = plt.rcParams['axes.prop_cycle'].by_key()['color']
fig, axes = plt.subplots(2, 1, figsize=(8, 4))
ax = axes[0]
ax.plot(t_target, d_target - 1.5, color=ncs[0], label='Real Oscillation')
ax.plot(t_poor, d_poor - 1.5, color=ncs[3], label='Initial Force Constant')
ax.set_xticklabels([])
ax.set_ylabel('Oscillation \n amplitude (Å)')
ax = axes[1]
const = 0.0005 / 1.75 
ax.plot(t_target, d_target - d_poor, 'k-', alpha=0.5, label='Distance difference')
for i, (x, y) in enumerate(zip(t_poor[::40], d_poor[::40])):
        dy = (np.abs(f[i, 0, 0]) + np.abs(f[i, 1, 0])) * np.sign(d_target[i * 40] - d_poor[i * 40]) * const
        dx = 0
        ax.arrow(x, 0, dx, dy, ec=ncs[2], head_width=2.75, head_length=0.10, fc=ncs[2])
ax.set_xlabel('Time (fs)')
ax.set_ylabel('Oscillation \n error (Å)')

for ax in axes:
    ax.set_xlim([0, 400])
#fig.tight_layout(h_pad=-10)
plt.savefig('xpot_example.pdf')     
