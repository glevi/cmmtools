''' Test that nonbonded energies calculated via ASEs CombineMM
    calculator gives the same results as when done with OpenMM '''
import numpy as np
import matplotlib.pyplot as plt
import ase.units as ase_u
from ase import Atoms
from ase.calculators.combine_mm import CombineMM
from ase.calculators.counterions import AtomicCounterIon as ACI
from simtk import unit as u
from simtk.openmm.openmm import NonbondedForce
from cmm.tools.ase_openmm import OpenMMCalc
from cmm.tools.tip4pew import angleHOH, rOH, TIP4PEW, sigma0 as sig_ew, epsilon0 as eps_ew

def make_omm_atoms(r=3):
    ''' Make the dimer for the OpenMM calc. '''
    x = angleHOH * np.pi / 180 / 2
    pos = [[0, r, 0],
           [0, 0, 0],
           [0, rOH * np.cos(x), rOH * np.sin(x)],
           [0, rOH * np.cos(x), -rOH * np.sin(x)]]

    tip4 = TIP4PEW().add_virtual_sites(np.array(pos[1:]))
    pos = np.vstack((pos[0], tip4))
    atoms = Atoms('IOH2He', positions=pos)
    return atoms


def get_lj(atoms, idx):
    ''' get LJ parameters from OpenMM atoms '''
    sigma = np.nan
    epsilon = np.nan
    for force in atoms.calc.sys.getForces():
        if isinstance(force, NonbondedForce):
            for i in range(force.getNumParticles()):
                if i == idx:
                    charge, sigma, epsilon = force.getParticleParameters(i)
                    sigma = sigma.in_units_of(u.angstrom)._value
                    epsilon = epsilon.in_units_of(u.kilojoule_per_mole)._value * ase_u.kJ / ase_u.mol

    return sigma, epsilon


def make_ase_atoms(sig, eps, r, chrg=-1, calc2=TIP4PEW, sig2=sig_ew, eps2=eps_ew):
    ''' Make ASE atoms object for CombineMM '''
    # Set up water box at 20 deg C density
    x = angleHOH * np.pi / 180 / 2
    pos = [[0, r, 0],
           [0, 0, 0],
           [0, rOH * np.cos(x), rOH * np.sin(x)],
           [0, rOH * np.cos(x), -rOH * np.sin(x)]]
    atoms = Atoms('IOH2', positions=pos)
    calc = CombineMM([0], apm1=1, apm2=3,
                     calc1=ACI(chrg, eps, sig),
                     calc2=calc2(),
                     sig1=[sig], eps1=[eps],
                     sig2=np.array([sig2, 0, 0]), eps2=np.array([eps2, 0, 0]),
                     rc=100)
    atoms.calc = calc
    return atoms

rs = np.arange(1.5, 10, 0.05)
omm_ener = np.zeros(len(rs))
ase_ener = np.zeros(len(rs))

for i, r in enumerate(rs):
    omm_atoms = make_omm_atoms(r)
    calc = OpenMMCalc([['I'], ['HOH', 'HOH', 'HOH', 'HOH']])
    calc.make_topology(omm_atoms)
    omm_atoms.calc = calc
    omm_ener[i] = omm_atoms.get_potential_energy()

    if i == 0:
        sigma, epsilon = get_lj(omm_atoms, 0)

    ase_atoms = make_ase_atoms(sigma, epsilon, r, calc2=TIP4PEW, sig2=sig_ew, eps2=eps_ew)
    ase_ener[i] = ase_atoms.get_potential_energy()

fig, ax = plt.subplots(1, 1, figsize=(8, 4))
ax.plot(rs, omm_ener, label='OpenMM')
ax.plot(rs, ase_ener, '--', color=f'C{3}', label='CombineMM' )
ax.set_ylim([-0.6, 1.5])
ax.set_xlim([2.5, 10])
ax.legend(loc='best', ncol=2)
ax.set_xlabel('r (Å)')
ax.set_ylabel('E (eV)')
fig.savefig('ASE_vs_OMM.pdf')
