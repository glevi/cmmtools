CMMTools
========

Package for CMM containing:
- Very very preliminary tools for using OpenMM (and more) to run MD simulations of small, solvated complexes
- Proof-of-concept implementations of WAXS-potiential & Forces. 
- g(r) -> S(q) code
- Debye scattering code
- LJ-Fitting tools


For an example figure visualising the WAXS-forces see `examples/xpot_example.py`


Some other examples are available in `examples/` but many still need to be added.

INSTALLATION
============
Clone the repository and put it in your `$PYTHONPATH`.  
I am not the best at keeping track of my dependencies, apologies, but the main libraries 
and how to get them is described below. 



Requirements and Installation (For DTU HPC):
============================================
But surely also elsewhere in a very similar fashion


It's easiest through conda:
---------------------------

miniconda installation - something like:  
`wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh`  
`bash ./Miniconda3-latest-Linux-x86_64.sh -b -p $HOME/miniconda3`  
`export PATH="$HOME/miniconda3/bin:$PATH"`  


And then to get to the miniconda base, and use that python dist for everything related to openMM

`unset PYTHONHOME`  
`unset PYTHONPATH`  (if you have something you need in here, then don't forget to add it again after)  
`conda activate`  

`git clone git@gitlab.com:asod/cmmtools.git <somewhere>`. Then add `<somewhere>` to your `$PYTHONPATH`  

other stuff needed / how to install:

`pip install ase`  (for the scattering tools)   
`conda install -c conda-forge parmed`  (for the MD tools. https://github.com/ParmEd/ParmEd/)    
`conda install -c conda-forge openmmtools`  (for the MD tools. https://openmmtools.readthedocs.io)    
`conda install -c conda-forge mdanalysis`  (for the MD tools. https://www.mdanalysis.org/pages/installation_quick_start/)  

(OpenMM is a dependency of openmmtools, so you get it that way...)


Test runs on GPU nodes:
-----------------------

To run test on a GPU node on HPC, you do 
`voltash`


instead of `linuxsh`. Then openMM should automatically pick the fastest (GPU) platform.


`module load cuda/10.2`  or get the CUDA software if you own a cuda-GPU. If not, fine...  


Even though the cuda module is loaded, it can happen that OpenMM cannot find it. 
That can be fixed with: 

`export OPENMM_CUDA_COMPILER=` `` `which nvcc` `` 


Important documents for actual use
----------------------------------
The tools require you to have produced AMBER-style `prmtop` topology files. For metal-containing 
complexes, some parametrization is required. 

The closest tutorial:  
http://ambermd.org/tutorials/advanced/tutorial20/mcpbpy_heme.htm

Useful manual:  
https://ambermd.org/doc12/Amber20.pdf


LEap 'manual':  
https://ambermd.org/tutorials/pengfei/index.php

A slightly different tutorial:  
http://ambermd.org/tutorials/advanced/tutorial20/mcpbpy.htm 


If you can get away with a nonbonded model and just put on some Metal-ligand constraints, this
might be easier:  
https://ambermd.org/tutorials/advanced/tutorial20/nonbonded_model.htm  
(it also might not)


If you have something organic that should be modelable with GAFF (no metals), look at:  
http://ambermd.org/tutorials/basic/tutorial4b/index.html  
and http://ambermd.org/antechamber/ac.html


Other important resources
-------------------------
https://openmmtools.readthedocs.io/en/0.18.1/forces.html  Custom forces

https://www.mdanalysis.org/  Good for trajectory analysis
