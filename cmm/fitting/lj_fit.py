import os
import numpy as np
from scipy.optimize import minimize, least_squares, basinhopping
from ase.calculators.tip4p import epsilon0, sigma0
from ase.calculators.acn import (sigma_c, sigma_me, sigma_n,
                                 epsilon_c, epsilon_me, epsilon_n)
from ase.calculators.qmmm import LJInteractionsGeneral
from ase import units
from ase.io import read, Trajectory
from cmm.fitting.parameters import (atoms_fet, atoms_ird, atoms_ptp,
                                   lj_parms_fet, lj_parms_ird, lj_parms_ptpop)

from pyswarms.single.global_best import GlobalBestPSO

kcalmol = units.kcal / units.mol

def standardize_f(x, xmin, xmax):
    return (x - xmin) / (xmax - xmin)

def standardize_b(xnorm, xmin, xmax):
    return xnorm * (xmax - xmin) + xmin


class MyBounds(object):
    def __init__(self, xmax=[1, 1, 1, 1, 1, 1, 1, 1], xmin=[0, 0, 0, 0, 0, 0, 0, 0]):
        self.xmax = np.array(xmax)
        self.xmin = np.array(xmin)
    def __call__(self, **kwargs):
        x = kwargs["x_new"]
        tmax = bool(np.all(x <= self.xmax))
        tmin = bool(np.all(x >= self.xmin))
        return tmax and tmin


class LJFit(object): 
    ''' Optimize QM/MM LJ Parameters to minimize the difference between 
        interaction energies  of a series of cluster geometries in an ASE 
        trajectory. 
        
        
        Steps:
           1. Load trajectory
           2. Load target interaction energies
           3. Load QM/MM interaction energies
           4. Define monomer indexes
           5. If LJ energy is already in QM/MM int. energies, remove
           6. FIT:
              a. Define which elements to fit LJ params of
              b. Define cost function 
              c. fit
       

        Input parameters:

            mono_idxs: list of lists, always arranged like:
                        [[indices of the solute], 
                         [indices of solvent1], [indices of solvent2], ...]

            trajectory: str or ase.io.trajectory.TrajectoryReader
                        path to ase trajectory of the geometries
                        or the trajectory itself

            solvent:    H2O or ACN
                        Defines which LJ parameters to use for the 
                        solvent


        '''


    def __init__(self, mono_idxs, trajectory=None, elements=['Pt', 'P', 'O', 'H'],
                 lam=0.1, solvent='H2O', comm=None, potential='LJ', atoms=None):
        self.elements = elements
        self.mono_idxs = mono_idxs
        self.method = 'L-BFGS-B'
        self.bounds = []
        self.lam = lam   # for regularization
        self.solvent = solvent
        self.weights = None
        self.comm = comm
        self.potential = potential

        if isinstance(trajectory, str):
            trajectory = Trajectory(trajectory, 'r')
        self.traj = trajectory

        # WIP: Atoms in system for later making the LJ object. This is rigid and bad.
        # The user should just put in the atoms object themselves. 
        if 'Pt' in elements:
            self.atoms = atoms_ptp
        elif 'Ir' in elements:
            self.atoms = atoms_ird
        elif 'Fe' in elements and len(mono_idxs[0]) == 59:  # To avoid potential chaos with FeBPY later
            self.atoms = atoms_fet
        elif atoms:
            self.atoms = atoms 
        else:
            raise RuntimeError('Did not understand input molecule')


    def load_target(self, target):
        if isinstance(target, str):  # assume txt file of Step#, E_dimer, E_mono1, E_mono2, ...
            data = np.genfromtxt(target)
            qmqm_int = data[:, 1] - np.sum(data[:, 2:], axis=1)
            self.target = qmqm_int
            self.steps = [int(x) for x in data[:, 0]]
        elif isinstance(target, np.ndarray):  # assume array of (steps, E_binding)
            self.target = target[:, 1]
            self.steps = target[:, 0]
        else:
            raise RuntimeError('Did not understand target')

        self.tlen = len(self.target)
    
    def load_atoms(self, steps=None):
        ''' Continuing through whole trajectory seems to
            slow the residual evaluation down immensely, 
            so load relevant steps in to list of atoms objs instead. '''
        if not steps:
            steps = self.steps
        atoms_list = [self.traj[int(step)] for step in steps]
        self.atoms_list = atoms_list

    def load_qmmm(self, fpath, lj=False):
        ''' Extract coulombic QM/MM interaction from QM/MM 
            data by subtracting the same LJ energy as was calculated
            during the QM/MM run. '''
        qmmm = np.genfromtxt(fpath)
        qmmm_int = qmmm[:, 1] - np.sum(qmmm[:, 2:], axis=1)
        # Subtract using a predefined LJ object 
        E = np.zeros(len(self.steps))
        if lj:
            E = self.calculate_lj(lj)

        self.coul = qmmm_int - E

    def set_mm_coulomb(self, coul):
        ''' Get MM Coulomb energy for frames in atom_list 
            if you are fitting and MM potential. 
            
            How: See e.g. get_energy in tm_tools.py in the 
            in https://gitlab.com/asod/tm-dispersion-bench 
            
            WIP: Get this functionality into here as well.
            '''

        if (coul == 0).all():
            print('**WARNING: ALL COULOMB ENERGIES WERE ZERO')

        self.coul = coul


    def calculate_lj(self, lj):
        E = np.zeros(len(self.steps))
        for i, atoms in enumerate(self.atoms_list):
            qmidx = self.mono_idxs[0]
            mmidx = sum(self.mono_idxs[1:], [])
            qmatoms = atoms[qmidx]
            mmatoms = atoms[mmidx]
            e, _, _  = lj.calculate(qmatoms, mmatoms, 0)
            E[i] = e

        return E 


    def loss(self, x, *args):
        res = self.residual(x)
        R = np.sum(np.sqrt(res**2) / len(x))

        if self.iter % 50 == 0:
            print(f'Iter: {self.iter:04d}, R: {R:g}')

        return R


    def residual(self, x):
        lj = self.make_lj_object(x)
        e_lj = self.calculate_lj(lj)

        e_predic = self.coul + e_lj
        e_target = self.target

        res = e_predic - e_target

        if self.iter % 50 == 0:
            print(f'Iter: {self.iter:04d}, Residual Sum: {sum(abs(res)):g}')

        self.iter += 1

        return res 
       

    def regu_loss(self, x, *args):
        ''' Regularized cauchy loss using standardized parameters, to 
            put into Basin Hopping optimizer. 
            
            Cauchy loss is stolen from least_squares: rho((f(x)) = ln(1 + f(x)) 
            given the residuals f(x) '''

        b = np.array(self.bounds)
        x_actual = standardize_b(x, b[:, 0], b[:, 1])

        lj = self.make_lj_object(x_actual)
        e_lj = self.calculate_lj(lj)


        e_predic = self.coul + e_lj
        e_target = self.target

        res = np.abs(e_predic - e_target)
        rho = np.sum(np.log(1 + res))


        # Regularization should be on standardized values!
        p = self.populate_dict(x)
        regu = 0
        for el in self.elements:
            if (el == 'Pt') or (el == 'Ir'):
                continue
            for i in range(2):
                regu += np.log(1 + np.abs(self.regupars[el][i] - p[el][i]))


        loss = rho + self.lam * regu
        
        if np.isnan(rho):  # if rho is nan, something is completely wrong and it should break
            raise RuntimeError('NaNs in your loss!')

        if self.iter % 50 == 0:
            print(f'Iter: {self.iter:04d}, Residual Sum: {rho:g}, Regu: {self.lam * regu:g}')

        self.iter += 1

        return loss


    def regu_sl1_loss(self, x, *args):
        ''' Regularized soft L1 loss using standardized parameters, to 
            put into Basin Hopping optimizer.

            Can be weighted with self.weights. Use e.g. 
            the absolute value of the interaction energy or some such.
            This is done since we want the BIG interaction values to be accurate, since that
            is where they matter the most.
            
            Soft L1 loss is stolen from least_squares: rho((f(x)) = 2 * ((1 + f(x))**0.5 - 1)
            given the residuals f(x) '''

        b = np.array(self.bounds)
        x_actual = standardize_b(x, b[:, 0], b[:, 1])
        
        lj = self.make_lj_object(x_actual)
        e_lj = self.calculate_lj(lj)

        e_predic = self.coul + e_lj
        e_target = self.target

        res = (e_predic - e_target)**2

        w = np.ones_like(e_target)
        if self.weights is not None:
            w = self.weights

        res *= w

        rho = np.sum((1 + res)**0.5 - 1) / self.tlen 
        
        # Regularization should be on standardized values!
        skip_regu = ['Pt', 'Ir']   # XXX WIP: Make more flexible
        skip = len([el for el in self.elements if el not in skip_regu])
        xlen = len(x) - skip  # for the 2 Me params 
        p = self.populate_dict(x)
        regu = 0
        for el in self.elements:
            if (el == 'Pt') or (el == 'Ir') or (el == 'Fe'):
                continue
            for i in range(2):
                regu += (1 + (self.regupars[el][i] - p[el][i])**2)**0.5 - 1

        regu /= xlen
        regu *= 1000
        rho *= 1000
        
        loss = rho + self.lam * regu

        if np.isnan(rho):  # if rho is nan, something is completely wrong and it should break
            raise RuntimeError('NaNs in your loss!')

        if self.iter % 50 == 0:
            rank = 0
            if self.comm:
                rank = self.comm.Get_rank()
            #print(f'Iter: {self.iter:04d}, Residual Sum: {rho:g}, Regu: {self.lam * regu:g}')
            idx = len(self.elements)
            print(f'RANK: {rank}. Iter: {self.iter:04d}, Res. Sum: {rho:7.4f}, Regu: {self.lam * regu:7.4f}' + 
                  f' Eps {self.elements[0]}: {x_actual[0]:7.5f}, Sig {self.elements[0]}: {x_actual[idx]:7.5f}')

        self.iter += 1

        return loss

    def regu_cauchy_loss(self, x, *args):
        ''' Regularized Cauchy using standardized parameters, to 
            put into Basin Hopping optimizer.

            Can be weighted with self.weights. Use e.g. 
            the absolute value of the interaction energy or some such.
            This is done since we want the BIG interaction values to be accurate, since that
            is where they matter the most.
            
            Cauchy loss is stolen from least_squares: rho((f(x)) = ln(1 + f(x)) 
            given the residuals f(x). 
            Whether or not it makes sense to add regularization term to this term 
            I don't actually know.  '''

        b = np.array(self.bounds)
        x_actual = standardize_b(x, b[:, 0], b[:, 1])

        lj = self.make_lj_object(x_actual)
        e_lj = self.calculate_lj(lj)

        e_predic = self.coul + e_lj
        e_target = self.target

        res = (e_predic - e_target)**2

        w = np.ones_like(e_target)
        if self.weights is not None:
            w = self.weights

        res *= w
        
        rho = np.sum(np.log(1 + res)) / self.tlen 

        # Regularization should be on standardized values!
        skip_regu = ['Pt', 'Ir']   # XXX WIP: Make more flexible
        skip = len([el for el in self.elements if el not in skip_regu])
        xlen = len(x) - skip  # for the 2 Me params 
        p = self.populate_dict(x)
        regu = 0
        for el in self.elements:
            if (el == 'Pt') or (el == 'Ir') or (el == 'Fe'):
                continue
            for i in range(2):
                regu += (1 + (self.regupars[el][i] - p[el][i])**2)**0.5 - 1

        regu /= xlen
        regu *= 1000
        rho *= 1000
        
        loss = rho + self.lam * regu

        if np.isnan(rho):  # if rho is nan, something is completely wrong and it should break
            raise RuntimeError('NaNs in your loss!')

        if self.iter % 50 == 0:
            rank = 0
            if self.comm:
                rank = self.comm.Get_rank()
            #print(f'Iter: {self.iter:04d}, Residual Sum: {rho:g}, Regu: {self.lam * regu:g}')
            idx = len(self.elements)
            print(f'RANK: {rank}. Iter: {self.iter:04d}, Res. Sum: {rho:7.4f}, Regu: {self.lam * regu:7.4f}' + 
                  f' Eps {self.elements[0]}: {x_actual[0]:7.5f}, Sig {self.elements[0]}: {x_actual[idx]:7.5f}')

        self.iter += 1

        return loss

    def swarm_residual(self, xes):
        #all_res = np.zeros((len(self.target), len(xes)))
        xeslen = len(xes)
        all_res = np.zeros(xeslen)

        tlen = len(self.atoms_list)
        for i, x in enumerate(xes):
            lj = self.make_lj_object(x)
            e_lj = self.calculate_lj(lj)

            e_predic = self.coul + e_lj

            res = e_predic - self.target
            #all_res[:, i] = res
            all_res[i] = np.sum((res**2)**0.5) / tlen
            #print(e_lj)
        
        return all_res

    def swarm(self, n_particles=10, iters=100, options={'c1': 0.5, 'c2': 0.3, 'w': 0.3}):
        ''' Particle Swarm Optimization 
            using PySwarm. 0
            TODO: Make a self.residual_featurescaled '''
        bounds = (np.array(list(b[0] for b in self.bounds)),
                  np.array(list(b[1] for b in self.bounds))) 
        if len(bounds[0]) == 0:
            bounds = None 
        else:
            xmin = tuple([x[0] for x in bounds])
            xmax = tuple([x[1] for x in bounds])
            bounds = (xmin, xmax)

        obj_fun = self.swarm_residual 

        print(bounds)

        dimensions = len(self.elements) * 2
        optimizer = GlobalBestPSO(n_particles=n_particles, options=options, dimensions=dimensions, bounds=bounds)
        cost, pos = optimizer.optimize(obj_fun, iters=iters)

        self.swopt = optimizer

        return (cost, pos)
    
    def basin_fit(self, x0, niter=50, stepsize=1e-1, interval=10, T=1500, loss=None, regutarget=None):
        self.iter = 0

        b = np.array(self.bounds)
        x0_std = standardize_f(x0, b[:, 0], b[:, 1])

        if 'Pt' in self.elements:
            ljp = lj_parms_ptpop
        elif 'Ir' in self.elements:
            ljp = lj_parms_ird
        elif 'Fe' in self.elements:
            ljp = lj_parms_fet
        elif regutarget:
            ljp = regutarget
        else:
            raise RuntimeError('Did not understand elements!')

        regu_x = np.array([ljp[el] for el in self.elements]).T.reshape(-1)
        regu_std_x = standardize_f(regu_x, b[:, 0], b[:, 1] )
        self.regupars = self.populate_dict(regu_std_x)


        mk = {"jac":False}

        scaled_bounds = np.zeros(b.shape)
        scaled_bounds[:, 1] = 1
        #sbounds = (np.array(list(0 for b in self.bounds)),
        #           np.array(list(1 for b in self.bounds)))
        mk['bounds'] = scaled_bounds

        # don't hop out of bounds
        bh_bounds = MyBounds(xmax=[1 for x in self.elements], xmin=[0 for x in self.elements])

        if not loss:
            loss = self.regu_loss

        opt = basinhopping(loss, x0_std,
                minimizer_kwargs=mk, stepsize=stepsize, niter=niter,
                disp=True, T=T, interval=interval, accept_test=bh_bounds)

        opt['x'] = standardize_b(opt['x'], b[:, 0], b[:, 1])

        self.opt = opt

        return opt


    def dual_anneal(self, loss=None, regutarget=None):
        self.iter = 0

        b = np.array(self.bounds)

        if 'Pt' in self.elements:
            ljp = lj_parms_ptpop
        elif 'Ir' in self.elements:
            ljp = lj_parms_ird
        elif 'Fe' in self.elements:
            ljp = lj_parms_fet
        elif regutarget:
            ljp = regutarget
        else:
            raise RuntimeError('Did not understand elements!')

        regu_x = np.array([ljp[el] for el in self.elements]).T.reshape(-1)
        regu_std_x = standardize_f(regu_x, b[:, 0], b[:, 1] )
        self.regupars = self.populate_dict(regu_std_x)

        scaled_bounds = np.zeros(b.shape)
        scaled_bounds[:, 1] = 1
        
        if not loss:
            loss = self.regu_loss
        
        from scipy.optimize import dual_annealing
        opt = dual_annealing(loss, scaled_bounds)
        opt['x'] = standardize_b(opt['x'], b[:, 0], b[:, 1])

        self.opt = opt

        return opt


    def fit(self, x0, args=()):
        bounds = self.bounds
        obj_fun = self.loss
        
        self.iter = 0
        if len(bounds) == 0:
            bounds = None

        opt = minimize(obj_fun, x0, bounds=bounds, method=self.method)
        self.opt = opt
        return opt


    def fit_nlls(self, x0):
        bounds = (np.array(list(b[0] for b in self.bounds)),
                  np.array(list(b[1] for b in self.bounds))) 
        if len(bounds[0]) == 0:
            bounds = None 

        obj_fun = self.residual

        self.iter = 0

        # feature scaling this is to the max set in bounds:
        x_scale = [b[1] for b in self.bounds]

        opt = least_squares(obj_fun, x0, bounds=bounds, loss='cauchy',
                            x_scale=x_scale, max_nfev=2000) 

        return opt

    def make_lj_object(self, x):
        p = self.populate_dict(x) 
        qmidx = self.mono_idxs[0]

        epsilon_qm = np.array([p[a.symbol][0] for a in self.atoms[qmidx]])
        sigma_qm = np.array([p[a.symbol][1] for a in self.atoms[qmidx]])
   
        if self.solvent == 'H2O':
            sig_mm = np.array([sigma0, 0, 0])
            eps_mm = np.array([epsilon0, 0, 0])
        elif self.solvent == 'ACN':
            sig_mm = np.array([sigma_me, sigma_c, sigma_n])
            eps_mm = np.array([epsilon_me, epsilon_me, epsilon_n])
        else:
            print('Solvent not understood!')
            raise ValueError 
        if self.potential == 'LJ':
            lj = LJInteractionsFast(sigma_qm, epsilon_qm, 
                                    sig_mm, eps_mm, 
                                    len(qmidx))
        elif self.potential == 'buf147':
            lj = Buf14_7(sigma_qm, epsilon_qm, 
                        sig_mm, eps_mm, 
                        len(qmidx))
        else:
            raise RuntimeError('Did not understand which potential to use')

        return lj

    def populate_dict(self, x):
        ''' fit tuples are (all epsilons, all sigmas)'''
        p = {el:(x1, x2) for (el, x1, x2) in
             zip(self.elements, x[:len(x) // 2], x[len(x) // 2:])}

        return p

    def print_results(self, fitted_params, ref=None, first='FIT', last='OPLS', ax=None):
        fitstyles = [first, last]
        if not ax:
            for i, fp in enumerate((fitted_params, ref)):
                print(fitstyles[i])
                for key, val in fp.items():
                    print('{0:2s}: eps: {1:12.8f} eV ({2:10.8f} kcal/mol), sigma: {3:7.5f} Ang'.format(key, val[0], val[0] / kcalmol, val[1]))
            print(f'DIFFERENCE ({first} - {last}):')
            for key in fitted_params.keys():
                eps_f = fitted_params[key][0] / kcalmol
                sig_f = fitted_params[key][1]
                eps_o = ref[key][0] / kcalmol
                sig_o = ref[key][1]
                print(f'{key:2s}: eps: {eps_f - eps_o:12.8f} kcal/mol, sig: {sig_f - sig_o:8.5f} Ang.')

        else:
            txt = ''
            for i, fp in enumerate((fitted_params, ref)):
                txt += fitstyles[i] + ":\n"
                for key, val in fp.items():
                    txt +=  '{0:2s}: eps: {1:12.8f} eV ({2:10.8f} kcal/mol), sigma: {3:7.5f} Ang\n'.format(key, val[0], val[0] / kcalmol, val[1]) 
            txt +=f'DIFFERENCE ({first} - {last}):\n'
            for key in fitted_params.keys():
                eps_f = fitted_params[key][0] / kcalmol
                sig_f = fitted_params[key][1]
                eps_o = ref[key][0] / kcalmol
                sig_o = ref[key][1]
                txt += f'{key:2s}: eps: {eps_f - eps_o:12.8f} kcal/mol, sig: {sig_f - sig_o:8.5f} Ang.\n'
            print(txt)
            
            ax.text(0.0, 0.5, txt, size=8, usetex=False)


def combine_lj_lorenz_berthelot(sigmaqm, sigmamm,
                                epsilonqm, epsilonmm):
    """Combine LJ parameters according to the Lorenz-Berthelot rule"""
    sigma = []
    epsilon = []
    # check if input is tuple of vals for more than 1 mm calc, or only for 1.
    if type(sigmamm) == tuple:
        numcalcs = len(sigmamm)
    else:
        numcalcs = 1  # if only 1 mm calc, eps and sig are simply np arrays
        sigmamm = (sigmamm, )
        epsilonmm = (epsilonmm, )
    for cc in range(numcalcs):
        sigma_c = np.zeros((len(sigmaqm), len(sigmamm[cc])))
        epsilon_c = np.zeros_like(sigma_c)

        for ii in range(len(sigmaqm)):
            sigma_c[ii, :] = (sigmaqm[ii] + sigmamm[cc]) / 2
            epsilon_c[ii, :] = (epsilonqm[ii] * epsilonmm[cc])**0.5
        sigma.append(sigma_c)
        epsilon.append(epsilon_c)

    if numcalcs == 1:  # retain original, 1 calc function
        sigma = np.array(sigma[0])
        epsilon = np.array(epsilon[0])

    return sigma, epsilon


def combine_lj_amoeba(sigmaqm, sigmamm,
                                epsilonqm, epsilonmm):
    """Combine LJ parameters according to the amoeba rules"""
    sigma = []
    epsilon = []
    #sigmaqm *=  2**(1/6) # convert to rmin
    #sigmamm *=  2**(1/6) 
    # check if input is tuple of vals for more than 1 mm calc, or only for 1.
    if type(sigmamm) == tuple:
        numcalcs = len(sigmamm)
    else:
        numcalcs = 1  # if only 1 mm calc, eps and sig are simply np arrays
        sigmamm = (sigmamm, )
        epsilonmm = (epsilonmm, )
    for cc in range(numcalcs):
        sigma_c = np.zeros((len(sigmaqm), len(sigmamm[cc])))
        epsilon_c = np.zeros_like(sigma_c)

        for ii in range(len(sigmaqm)):
            sigma_c[ii, :] = (sigmaqm[ii]**3 + sigmamm[cc]**3) / (sigmaqm[ii]**2 + sigmamm[cc]**2)
            epsilon_c[ii, :] = 4 * (epsilonqm[ii] * epsilonmm[cc]) / (epsilonqm[ii]**0.5 * epsilonmm[cc]**0.5)**2
        sigma.append(sigma_c)
        epsilon.append(epsilon_c)

    epsilon = np.nan_to_num(epsilon)
    if numcalcs == 1:  # retain original, 1 calc function
        sigma = np.array(sigma[0])
        epsilon = np.array(epsilon[0])
    
    #sigma /=  2**(1/6) # convert back
    return sigma, epsilon


class LJInteractionsFast:
    name = 'LJ-general'

    def __init__(self, sigmaqm, epsilonqm, sigmamm, epsilonmm,
                 qm_molecule_size, mm_molecule_size=3):
        """General Lennard-Jones type explicit interaction.

           Taken from ase.calculators.qmmm but gutted for speed. 
           no: Forces, cutoff, PBC. (gave ~ 2x speedup) 

           Tested to give identical energies to the original

        """
        self.sigmaqm = sigmaqm
        self.epsilonqm = epsilonqm
        self.sigmamm = sigmamm
        self.epsilonmm = epsilonmm
        self.qms = qm_molecule_size
        self.mms = mm_molecule_size
        self.combine_lj()

    def combine_lj(self):
        self.sigma, self.epsilon = combine_lj_lorenz_berthelot(
            self.sigmaqm, self.sigmamm, self.epsilonqm, self.epsilonmm)

    def calculate(self, qmatoms, mmatoms, shift):
        epsilon = self.epsilon
        sigma = self.sigma

        # loop over possible multiple mm calculators
        # currently 1 or 2, but could be generalized in the future...
        apm1 = self.mms
        mask1 = np.ones(len(mmatoms), dtype=bool)
        mask2 = mask1
        apm = (apm1, )
        sigma = (sigma, )
        epsilon = (epsilon, )

        mask = (mask1, mask2)
        e_all = 0

        # zip stops at shortest tuple so we dont double count
        # cases of no counter ions.
        for n, m, eps, sig in zip(apm, mask, epsilon, sigma):
            mmpositions = self.update(qmatoms, mmatoms[m], n, shift)
            energy = 0.0

            qmpositions = qmatoms.positions.reshape((-1, self.qms, 3))

            for qmpos in qmpositions:  # molwise loop
                # cutoff from first atom of each mol
                for qa in range(len(qmpos)):
                    if ~np.any(eps[qa, :]):
                        continue
                    R = mmpositions - qmpos[qa, :]
                    d2 = (R**2).sum(2)
                    c6 = (sig[qa, :]**2 / d2)**3
                    c12 = c6**2
                    e = 4 * eps[qa, :] * (c12 - c6)
                    energy += e.sum()

                e_all += energy

        return e_all, 0, 0


    def update(self, qmatoms, mmatoms, n, shift):
        positions = mmatoms.positions.reshape((-1, n, 3)) + shift

        return positions



class Buf14_7:
    name = 'Buffered 14-7'

    def __init__(self, sigmaqm, epsilonqm, sigmamm, epsilonmm,
                 qm_molecule_size, mm_molecule_size=3, combine='lb'):
        """ Amoeba Buffered 14-7 Potential

        """
        self.sigmaqm = sigmaqm
        self.epsilonqm = epsilonqm
        self.sigmamm = sigmamm
        self.epsilonmm = epsilonmm
        self.qms = qm_molecule_size
        self.mms = mm_molecule_size
        self.combine_lj(combine)

    def combine_lj(self, combine):
        if combine == 'lb':
            self.sigma, self.epsilon = combine_lj_lorenz_berthelot(
                self.sigmaqm, self.sigmamm, self.epsilonqm, self.epsilonmm)
        elif combine == 'amoeba':
            self.sigma, self.epsilon = combine_lj_amoeba(
                self.sigmaqm, self.sigmamm, self.epsilonqm, self.epsilonmm)

    def calculate(self, qmatoms, mmatoms, shift):
        epsilon = self.epsilon
        sigma = self.sigma

        # loop over possible multiple mm calculators
        # currently 1 or 2, but could be generalized in the future...
        apm1 = self.mms
        mask1 = np.ones(len(mmatoms), dtype=bool)
        mask2 = mask1
        apm = (apm1, )
        sigma = (sigma, )
        epsilon = (epsilon, )

        mask = (mask1, mask2)
        e_all = 0

        # zip stops at shortest tuple so we dont double count
        # cases of no counter ions.
        for n, m, eps, sig in zip(apm, mask, epsilon, sigma):
            mmpositions = self.update(qmatoms, mmatoms[m], n, shift)
            energy = 0.0

            qmpositions = qmatoms.positions.reshape((-1, self.qms, 3))

            for qmpos in qmpositions:  # molwise loop
                # cutoff from first atom of each mol
                for qa in range(len(qmpos)):
                    if ~np.any(eps[qa, :]):
                        continue
                    R = mmpositions - qmpos[qa, :]
                    d2 = (R**2).sum(2)
                    #c6 = (sig[qa, :]**2 / d2)**3
                    #c12 = c6**2
                    #e = 4 * eps[qa, :] * (c12 - c6)
                    rho = np.sqrt(d2) / sig[qa, :]
                    e = 4 * eps[qa, :] * (1.07 / (rho + 0.07))**7 * ((1.12 / (rho**7) + 0.12) - 2 )
                    energy += e.sum()

                e_all += energy

        return e_all, 0, 0


    def update(self, qmatoms, mmatoms, n, shift):
        positions = mmatoms.positions.reshape((-1, n, 3)) + shift

        return positions
