import itertools
import numpy as np
import MDAnalysis as mda
import matplotlib.pyplot as plt
from MDAnalysis.analysis import rdf
import rmsd
from ase import Atoms, units
from ase.io.cube import write_cube

''' (Will at some point become) a selection of tools for analysing MD 
    trajectories made with CMMSystems ''' 


def make_colors(c, colmap='viridis'):
    import matplotlib.pyplot as plt
    cmap = plt.get_cmap(colmap)
    colors = [cmap(1. * i / c) for i in range(c)]
    return colors


def get_rdf(fname, sel1, sel2, dr=0.05, rrange=(0.0, 30.0)):
    ''' Calculate pairwise RDF. 
        sel1:   Selection1 from an MDAnalysis.Universe()
        sel2:   Selection2 
    '''

    nbins = len(np.arange(rrange[0], rrange[1], dr))
    
    out = np.zeros((nbins, 2))
    irdf = rdf.InterRDF(sel1, sel2, nbins=nbins, range=rrange)
    irdf.run()
    
    out[:, 0] = irdf.bins
    out[:, 1] = irdf.rdf
    if fname:
        np.savetxt(fname, out)

    return out    


def plot_bond_hist(c_idx, u, atoms=None, fig=None):
    combs = list(itertools.combinations(c_idx, 2))
    steps = len(u.trajectory)
    num_dists = len(combs)

    u_names = []
    for (i, j) in combs:
        name = u.atoms[i].element + '-' + u.atoms[j].element
        u_names.append(name)

    dists = np.zeros((steps, num_dists))
    names = []
    vds = []
    for k, (i, j) in enumerate(combs):
        for f, frame in enumerate(u.trajectory):
            pos = frame.positions
            dists[f, k] = np.linalg.norm(pos[i] - pos[j])
        if atoms:
            vac_dist = atoms.get_distance(i, j)
            name = '-'.join(a for a in atoms[[i,j]].get_chemical_symbols())
            names.append(name)
            vds.append(vac_dist)
            print(f'Idx: {i}, {j}, Name: {u_names[k]:5s}: Avg Dist: {np.mean(dists[:, k]):6.4f} Vacuum: {name}: {vac_dist:6.4f}')
        

    x = np.linspace(min(dists.ravel()) - 0.1, max(dists.ravel()) + 0.1, 500)
    dx = np.diff(x)[0]
    col = make_colors(len(combs))

    if not fig:
        fig, ax = plt.subplots(1, 1, figsize=(8, 4))
    else:
        ax = fig.axes[0]
    for d, dist in enumerate(dists.T):
        y, _ = np.histogram(dist, x, density=True)
        ax.plot(x[:-1] - 0.5 * dx, y, color=col[d], label=u_names[d])
        if atoms:
            ax.plot([vds[d], vds[d]], [0, max(y)], '--', color=col[d])

    return ax

def plotcage_new(s, vdv, title):
    ''' see ../examples/xray/displaced_volume_scattering.ipynb'''
    fig, axes = plt.subplots(2, 1, figsize=(7, 4)) 
    ax = axes[0]
    ax.plot(s['q'], s['s_vdv_damp'], '-', color='C0', label='DV Solvent')
    ax.plot(s['q'], s['s_c_damp'], '-', color='C3', label='Cross')
    ax.plot(s['q'], s['s_c_damp'] +  s['s_vdv_damp'], '--', color='k', label='Sum: Cage')
    
    ax.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax.set_xlim([0, 8])
    ax.set_xticklabels('')
    ax.legend(loc='right')
    ax.set_ylabel('S(q) (e.u)')
    ax = axes[1]
    ax.plot(s['q'], vdv, '-', color='C3', label='$f_\mathrm{DV}:$' +f' {title}')
    
    ax.set_xlim([0, 8])
    ax.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    ax.legend(loc='right')
    ax.set_xlabel('q (Å$^{-1}$)')
    ax.set_ylabel('$f_\mathrm{DV}$ (e.u)')
    for ax in axes:
        ax.grid()
    fig.suptitle(title)
    
    rdf_list = [rdf for rdf in s['rdf'] if (rdf['name'].count('_u' ) == 2) and
                (rdf['name'].count(title) == 2)]
    if len(rdf_list) > 0:
        rdf = rdf_list[0]
        ax2 = axes[1].twinx() 
        
        ax2.plot(rdf['r']/2., rdf['gr'], label=rdf['name'])

    

class Sheller:
    ''' 
        3D Histograms of solvent densities around aligned solutes. 
        
        Parameters
        -----------
        universe :   MDAnalysis Universe object. 
                     Universe with trajectory to sample from. 

        name:       str 
                    name of output cube files
        solutesel:  str
                    MDAnalysis selection string of solute atoms to align to

        Usage example:

        <load your trajectory into an MDAnalysis universe u>
        >>> shl = Sheller(u, name='my_fancy_binning', solutesel='resid 1:7')
        >>> shl.make_array(watersel='type OW')
        >>> O, Oedges = shl.bin3d(shl.v_pos, bins=(151, 151, 151), lim=9)

    '''
    def __init__(self, universe, name='febpyshell', solutesel='resid 1:4'):
        self.u = universe
        self.name = name
        self.solusel = solutesel
        
        # frame to align to: Solute in frame 0 of the universe:
        solute = universe.select_atoms(solutesel)
        self.center = rmsd.centroid(solute.positions)
        self.ref_frame = solute.positions - self.center
        
    def make_array(self, watersel='type OW or type HW', start=0, end=-1, step=1):
        u = self.u
        water = u.select_atoms(watersel)
        solute = u.select_atoms(self.solusel)
        
        v_pos = np.zeros((len(u.trajectory[start:end:step]), len(water), 3))
        u_pos = np.zeros((len(u.trajectory[start:end:step]), len(solute), 3))
        for f, frame in enumerate(u.trajectory[start:end:step]):
            this_vpos = water.positions
            this_upos = solute.positions
            
            ali_u, ali_v = self.align(this_upos, this_vpos)
            v_pos[f, :, :] = ali_v
            u_pos[f, :, :] = ali_u
        
        self.v_pos = v_pos
        self.u_pos = u_pos
        
    def align(self, solu_pos, solv_pos):
        this_cen = rmsd.centroid(solu_pos)
        centered_v = solv_pos - this_cen
        centered_u = solu_pos - this_cen
        # Get rotmat from solute
        U = rmsd.kabsch(self.ref_frame, centered_u)
        # Apply it
        aligned_u = np.dot(centered_u, U.T)
        aligned_v = np.dot(centered_v, U.T)
        
        return aligned_u, aligned_v
        
    def bin3d(self, pos, bins=(81, 81, 81), normed=True, lim=15):
        ''' 3D bins the (Nwatermol x Nframes, 3) array "pos" 
            The center atom (e.g. Fe in FeBPY needs to be 
            in (0, 0, 0) ). 
            
            The atoms in the cube will be the avg aligned positions
            of the solute.
            
        '''
        
        flatpos = pos.reshape(-1, 3)
        name = self.name
        
        solute_elements = self.u.select_atoms(self.solusel).elements
        mean_solu_pos = np.mean(self.u_pos, axis=0)
        atoms = Atoms(''.join(el for el in solute_elements),
                      positions=mean_solu_pos)
        blim = lim / units.Bohr
        atoms.set_cell((blim, blim, blim))
        atoms.translate([blim, blim, blim])
        
        self.atoms = atoms
        
        O, Oedges = np.histogramdd(flatpos, 
                                  bins=bins, 
                                  range=((-lim, lim),(-lim, lim),(-lim, lim)), density=normed)

        with open(name + '.cube', 'w') as f:
            write_cube(f, atoms, data=O, origin=np.array([blim / 2, blim / 2, blim / 2]))

        return O, Oedges 