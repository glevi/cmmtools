import numpy as np
from ase import Atoms
from ase.calculators.calculator import Calculator, all_changes
from simtk import unit as u
from simtk.openmm.app.topology import Topology
from simtk.openmm.app import element, CutoffNonPeriodic, Simulation, PDBFile
from simtk.openmm.app.forcefield import ForceField
from simtk.openmm.openmm import NonbondedForce
from simtk import openmm as mm

def sys_as_ase(sys, ns_idx):
    ''' Convert CMMSystem atoms in ns_idx to ase Atoms object '''
    _, syms = sys.get_nonsolvent_indices()
    els = [atom.element._symbol for a, atom in enumerate(sys.topology.atoms()) if a in ns_idx]
    pos = np.array(sys.positions._value)[ns_idx]
    atoms  = Atoms(els, positions=pos)
    return atoms


class OpenMMCalc(Calculator):
    ''' Simple OpenMM ASE interface. '''
    implemented_properties = ['energy', 'forces']
    default_parameters = {'input' : "openmm.pdb",
                          'nonbondedMethod' : CutoffNonPeriodic,
                          'nonbondedCutoff' : 10 * u.angstrom,
                          'forcefield': 'amber14/tip4pew.xml'}

    def __init__(self, resnames, **kwargs):
        ''' OpenMM ASE interface. '''
        Calculator.__init__(self, **kwargs)  # gives self.parameters
        self.resnames = resnames
        self.top = None
        self.sys = None
        self.sim = None

    def make_topology(self, atoms, resnames=None):
        '''
        atoms:      ASE Atoms object
        resnames:   List of lists of residuenames,
                    e.g. for an iodide-TIP4P dimer:
                        [['I'], ['HOH', 'HOH', 'HOH', 'HOH']] '''
        top = Topology()
        symbols = atoms.get_chemical_symbols()
        atom_ct = 0
        if not resnames:
            resnames = self.resnames
        for residue in resnames:
            chain = top.addChain()
            res = top.addResidue(residue[0], chain=chain)
            res.insertionCode = ' '
            h_ct = 0
            for resname in residue:
                sym = symbols[atom_ct]
                ele = element.get_by_symbol(sym)
                name = sym
                if sym == 'He':
                    ele = None
                    name = 'EPW'
                if (resname == 'HOH') and sym == 'H':
                    h_ct += 1
                    name = f'H{h_ct}'

                top.addAtom(name, ele, res)
                atom_ct += 1

        top.createStandardBonds()
        self.top = top

    def topology_from_pdb(self, pdbfile):
        self.top = PDBFile(pdbfile).topology

    def set_lj(self, idx, eps, sig):
        for force in self.sys.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumParticles()):
                    if i == idx:
                        charge, sig_old, eps_old = force.getParticleParameters(i)
                        force.setParticleParameters(i, charge,
                                                   sig.in_units_of(u.nanometer),
                                                   eps.in_units_of(u.kilojoule_per_mole))

    def init_system(self, atoms=None):
        if self.top is None:
            self.make_topology(atoms, self.resnames)

        forcefield = ForceField(self.parameters['forcefield'])
        system = forcefield.createSystem(self.top, nonbondedCutoff=1 * u.nanometer,
                                         rigidWater=True)

        self.sys = system

    def init_simulation(self):
        platform = mm.Platform.getPlatformByName('Reference')
        integrator = mm.VerletIntegrator(0.5)  # Never used but needed for context
        simulation = Simulation(self.top, self.sys, integrator, platform)
        self.sim = simulation

    def calculate(self, atoms=None,
                  properties=['energy', 'forces'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        if self.sys is None:
            self.init_system(atoms)
        if self.sim is None:
            self.init_simulation()
        self.sim.context.setPositions(atoms.positions / 10)  # openmm uses nm
        gf = False
        if 'forces' in properties:
            gf = True
        state = self.sim.context.getState(getEnergy=True, getForces=gf)
        e = state.getPotentialEnergy().value_in_unit(u.kilocalories_per_mole) * ase_u.kcal / ase_u.mol
        f = np.zeros((len(atoms), 3))  # WIP manual force redist if you have TIP4P.
        if gf:
            f = state.getForces().value_in_unit(u.kilojoules_per_mole / u.angstrom)
            f = np.array(f) * ase_u.kJ / ase_u.mol
            self.results['forces'] = f

        self.results['energy'] = e
