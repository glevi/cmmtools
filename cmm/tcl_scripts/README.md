TCL Scripts for VMD
===================

These scripts are super clunky, and made to get things working with the minimum amount of TCL skills. 
I am neither good at tcl, nor do I like using it...

They can be run without any GUI with `vmd -dispdev none -e <script.tcl>` 

To make VMD respect CPU counts when submitting to clusters, export `VMDFORCECPUCOUNT` to the chosen number
in your submit script
