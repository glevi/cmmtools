import os
import numpy as np
from sys import stdout
from simtk.unit import md_unit_system
from simtk.openmm import app, Platform
from simtk.openmm.vec3 import Vec3
from simtk.openmm.openmm import (NonbondedForce, HarmonicBondForce,
                                 HarmonicAngleForce, PeriodicTorsionForce,
                                 MonteCarloBarostat, LangevinIntegrator,
                                 CustomExternalForce)
import simtk.unit as u
from simtk import openmm as mm
from parmed import load_file
from parmed.openmm.reporters import RestartReporter
from openmmtools.testsystems import (TestSystem, DEFAULT_CUTOFF_DISTANCE,
                                     DEFAULT_EWALD_ERROR_TOLERANCE,
                                     DEFAULT_SWITCH_WIDTH)

# WIP: Get this one up and running as well:
#from openmmtools.forces import FlatBottomRestraintForce
import MDAnalysis as MDA
from MDAnalysis.transformations import wrap

class CMMSystem(TestSystem):

    """ Class designed to do all the normal things we do to
        whatever poor small (TM) molecule in solution.

        NB! Remember to set constraints and forces BEFORE init'ing simulation
        object.

        Currently needs already parametrized .prmtop and .crd coords


        Would be nice to make it able to solvate by itself at some point.

        Parameters
        -----------
        tag : str,      will be trunk of all output files

        TBD . """



    def __init__(self, tag, fpath='./',  constraints=app.HBonds,
                 nonbonded_cutoff=DEFAULT_CUTOFF_DISTANCE,
                 ewald_error_tolerance=DEFAULT_EWALD_ERROR_TOLERANCE,
                 nonbonded_method=app.PME,
                 switch_width=DEFAULT_SWITCH_WIDTH,
                 **kwargs):

        TestSystem.__init__(self, **kwargs)

        self.tag = tag
        self.fpath = fpath
        os.makedirs(fpath, exist_ok=True)

        self.nbc = nonbonded_cutoff
        self.ert = ewald_error_tolerance
        self.nbm = nonbonded_method
        self.sww = switch_width

        self.con = constraints
        self.boxvectors = None
        self.velocities = None
        self.simulation = None


    def system_from_prmtop(self, prmtop_file, inpcrd=None):


        self.prmtop_file = prmtop_file

        prmtop = app.AmberPrmtopFile(prmtop_file)

        system = prmtop.createSystem(nonbondedMethod=self.nbm, nonbondedCutoff=self.nbc,
                                     constraints=self.con)

        self.system = system
        self.topology = prmtop.topology

        if inpcrd:
            inpcrd = app.AmberInpcrdFile(inpcrd)
            positions = inpcrd.getPositions(asNumpy=True)
            self.positions = positions

    def update_positions(self, numpy_ang_array):
        ''' Updates the positions from a numpy (N,3) array
            in angstrom. Does NOT update context, so do BEFORE init_simulation() '''
        pos = numpy_ang_array
        vec3_pos = [[] for x in range(len(pos))]
        for i, p in enumerate(pos):
            vec3_pos[i] = Vec3(*p) * u.angstroms
        self.positions = vec3_pos

    def pos_from_dcd(self, dcd_file, frame=-1, rewrap=False):
        ''' Updates self.positions from dcd_file frame'''
        uni = MDA.Universe(self.prmtop_file)
        uni.load_new(dcd_file)
        if rewrap:  # WIP, not properly tested
            transform = wrap(uni.atoms, compound='residues')
            uni.trajectory.add_transformations(transform)
        frame = uni.trajectory[frame]
        pos = frame.positions
        vec3_pos = [[] for x in range(len(pos))]
        for i, p in enumerate(pos):
            vec3_pos[i] = Vec3(*p) * u.angstroms

        self.positions = vec3_pos

        # boxvectors
        x, y, z = uni.dimensions[:3]
        boxvectors = (Vec3(x, 0, 0) * u.angstrom,
                      Vec3(0, y, 0) * u.angstrom,
                      Vec3(0, 0, z) * u.angstrom)
        self.boxvectors = boxvectors


    def load_rst7(self, rst7_file):
        """ Loads positions, velocities and box vectors from rst7 """

        rst = load_file(rst7_file)
        self.positions = rst.positions

        self.velocities = [vel * u.angstrom / u.picosecond for vel in rst.velocities]

        x, y, z = rst.box[:3]
        boxvectors = (Vec3(x, 0, 0) * u.angstrom,
                      Vec3(0, y, 0) * u.angstrom,
                      Vec3(0, 0, z) * u.angstrom)
        self.boxvectors = boxvectors

    def get_nonbonded(self, idx):
        ''' Return current nonbonded params '''
        charges = []
        sigmas = []
        epsilons = []

        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumParticles()):
                    if i not in idx:
                        continue
                    charge, sigma, epsilon = force.getParticleParameters(i)
                    charges.append(charge)
                    sigmas.append(sigma)
                    epsilons.append(epsilon)

        return charges, sigmas, epsilons

    def adjust_masses(self, idx, new_masses):
        ''' Change masses in the idx list to the list of masses in new_masses. '''

        if type(new_masses[0]) != u.Unit:  #  add units, assuming dalton (g/mol)
            new_masses = [nm * u.dalton for nm in new_masses]

        old_m = [self.system.getParticleMass(i) for i in idx]
        for i in idx:
            self.system.setParticleMass(i, new_masses[i])
            print(f'OLD: {old_m[i]._value:6.4f}, NEW: {new_masses[i]._value:6.4f}')

    def lj_opls_to_amber(self, sigma):
        ''' OPLS uses sigma-form LJ potential,
            AMBER uses Rmin. http://docs.openmm.org/latest/userguide/theory.html

            Rmin = 2**(1/6) * sigma '''
        return sigma * 2**(1 / 6)

    def adjust_lj(self, idx, new_lj, opls=False):
        ''' Sets new LJ parameters based on the new_lj.

            new_lj should be a dict with all uppercase keys
            of atom names, and tuples of (eps, sigma) values,
            in kcal/mol and angstrom units, e.g.:

                {'Fe':(0.00649996 * kilocalorie_per_mole, 1.29700 * angstroms)}

            opls: True: converts sigma to Rmin as is used in AMBER.
                  However, from looking at the sigma values for tip4pew
                  in the NonbondedForce object, it seems like openMM
                  ALSO uses sigma values. So most likely, this should
                  not be done.

        '''

        syms = [a.name for i, a in enumerate(self.topology.atoms())
               if i in idx]

        # Fill lists with new parameters from dict
        epsilons = []
        sigmas = []
        for sym in syms:
            typ = ''.join(s for s in sym if not s.isdigit())
            eps, sig = new_lj[typ]
            if opls:
                sig = self.lj_opls_to_amber(sig)
            epsilons.append(eps)
            sigmas.append(sig)

        ct = 0
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumParticles()):
                    if i not in idx:
                        continue
                    charge, sigma, epsilon = force.getParticleParameters(i)
                    # pylint: disable=maybe-no-member
                    force.setParticleParameters(i, charge * u.elementary_charge,
                                                sigmas[ct].in_units_of(u.nanometer),
                                                epsilons[ct].in_units_of(u.kilojoule_per_mole))
                    print(f'SIG: OLD: {sigma._value:9.6f}, NEW: {sigmas[ct].in_units_of(u.nanometer)._value:9.6f} ' +
                          f'EPS: OLD: {epsilon._value:9.6f}, NEW: {epsilons[ct].in_units_of(u.kilojoule_per_mole)._value:9.6f}')
                    ct += 1

        ### 1-4 SCALING: Only for AMBER - TODO: make it possible to turn it off.
        # The original LJ parameters in these force terms are not scaled, but simply
        # Lorenz-Berthelot-combined. We do the same! Epsilon is LB combined and scaled by 1/2.
        # We also do the same
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumExceptions()):
                    particle1, particle2, chargeProd, sigma, epsilon = force.getExceptionParameters(i)
                    if (particle1 in idx) & (particle2 in idx):
                        if chargeProd != 0 * u.elementary_charge**2: # Is it a 1-4 interaction?
                            new_sigma = (sigmas[particle1] + sigmas[particle2]) / 2
                            new_epsil = np.sqrt(epsilons[particle1] * epsilons[particle2]) / 2
                            force.setExceptionParameters(i, particle1, particle2,
                                                         chargeProd,
                                                         new_sigma.in_units_of(u.angstrom),
                                                         new_epsil.in_units_of(u.kilojoule_per_mole))



    def adjust_solute_charges(self, idx, new_charges, scale_fac=1.2):
        ''' idx: indices of solute, corresponding to new_charges '''
        ### MANUALLY ADJUST
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumParticles()):
                    if i not in idx:
                        continue
                    charge, sigma, epsilon = force.getParticleParameters(i)
                    force.setParticleParameters(i, new_charges[i] * u.elementary_charge, sigma, epsilon)
                    print(f'OLD: {charge._value:9.6f}, NEW: {new_charges[i]:9.6f}')

        ### 1-4 SCALING: Only for AMBER - TODO: make it possible to turn it off.
        for force in self.system.getForces():
            if isinstance(force, NonbondedForce):
                for i in range(force.getNumExceptions()):
                    particle1, particle2, chargeProd, sigma, epsilon = force.getExceptionParameters(i)
                    if (particle1 in idx) & (particle2 in idx):
                        if chargeProd != 0 * u.elementary_charge**2: # Is it a 1-4 interaction?
                            newChargeProd = new_charges[particle1] * new_charges[particle2] / scale_fac
                            force.setExceptionParameters(i, particle1, particle2,
                                                         newChargeProd * u.elementary_charge**2, sigma, epsilon)

    def add_constraint(self, i, j, dist):
        """ dist should be in angstrom """
        self.system.addConstraint(i, j, dist)

    def check_bonds(self, idx):
        ''' Return all bonds of atoms in the idx list '''
        for force in self.system.getForces():
            if isinstance(force, HarmonicBondForce):
                hbf = force
        syms = [a.name for a in self.topology.atoms()]
        bonds = []
        for bond in range(hbf.getNumBonds()):
            a, b, this_d, this_k = hbf.getBondParameters(bond)
            if (a in idx) or (b in idx):
                d_ang = this_d.in_units_of(u.angstroms)._value
                k_kjmolang = this_k.in_units_of(u.kilojoule_per_mole / u.angstroms**2)._value
                print(f'{a:2d},{b:2d}: {syms[a]:2s}-{syms[b]:2s}: Dist: {d_ang:6.4f} Ang. k = {k_kjmolang:g} kj/mol*ang**2')
                bonds.append((a, b, d_ang, k_kjmolang, syms[a] + syms[b]))

        if not bonds:
            print('No bonds found')

        return bonds


    def add_bond(self, i, j, dist, k):
        ''' Simple hookean bond with spring constant k between
            atoms of indices i and j '''
        bond = HarmonicBondForce()
        bond.addBond(i, j, dist, k)
        self.system.addForce(bond)

    def update_bond(self, i, j, dist=None, k=None, do_print=True):
        ''' Update already existing bond distance and/or spring constant '''
        syms = [a.name for a in self.topology.atoms()]

        for force in self.system.getForces():
            if isinstance(force, HarmonicBondForce):
                hbf = force
        exist = False
        for bond in range(hbf.getNumBonds()):
            a, b, this_d, this_k = hbf.getBondParameters(bond)
            if (a == i and b == j) or (b == i and a == j):
                exist = True
                if dist:
                    hbf.setBondParameters(bond, a, b, dist, this_k)
                if k:
                    hbf.setBondParameters(bond, a, b, this_d, k)
                if dist and k:
                    hbf.setBondParameters(bond, a, b, dist, k)
        if (not exist) and do_print:
            print(f'Bond between {syms[i]}-{syms[j]} didn\'t exist, please use add_bond')
        elif exist:
            print(f'Bond between {syms[i]}-{syms[j]} updated to {k}')

        if self.simulation.context:
            print('UPDATING CONTEXT')
            hbf.updateParametersInContext(self.simulation.context)

    def get_bond(self, i, j):
        ''' Get bond parameters between i and j '''
        syms = [a.name for a in self.topology.atoms()]

        this_d = None
        this_k = None
        for force in self.system.getForces():
            if isinstance(force, HarmonicBondForce):
                hbf = force
        for bond in range(hbf.getNumBonds()):
            a, b, d, k = hbf.getBondParameters(bond)
            if (a == i and b == j) or (b == i and a == j):
                this_d = d
                this_k = k
        if not this_d:
            print(f'Bond between {syms[i]}-{syms[j]} didn\'t exist, please use add_bond')

        return this_d, this_k, syms[a] + syms[b]

    def update_angle(self, i, j, k, angle=None, kf=None, do_print=True):
        ''' Update angle params between atoms a, b, c.
            Dont forget to use Quantities when setting. '''
        syms = [a.name for a in self.topology.atoms()]
        for force in self.system.getForces():
            if isinstance(force, HarmonicAngleForce):
                abf = force
        exist = False
        for bond in range(abf.getNumAngles()):
            a, b, c, this_angle, this_k = abf.getAngleParameters(bond)
            if (a == i) and (b == j) and (c == k):
                exist = True
                #print(this_angle, this_k)
                if angle:
                    abf.setAngleParameters(bond, a, b, c, angle, this_k)
                if kf:
                    abf.setAngleParameters(bond, a, b, c, this_angle, kf)
                if angle and kf:
                    abf.setAngleParameters(bond, a, b, c, angle, kf)
        if (not exist) and do_print:
            print(f'Angle between {syms[i]}-{syms[j]}-{syms[k]} didn\'t exist')
        elif exist:
            print(f'Angle between {syms[i]}-{syms[j]}-{syms[k]} updated')
        for force in self.system.getForces():
            if isinstance(force, HarmonicAngleForce):
                print('Updating object')
                force = abf


    def restrain_atoms(self, indices, k=500, pos=None):
        ''' Restrain atoms with indices to the positions in self
            or pos, with the Hookean constant k, in units of  kJ/(mol * ang**2)

            TODO: Flat-bottomed, or make a sphere-center version. Or both.

        '''

        k *= u.kilojoule_per_mole / u.angstrom**2

        energy_expression = '(K/2)*periodicdistance(x, y, z, x0, y0, z0)^2' # periodic distance
        restraint_force = CustomExternalForce(energy_expression)
        restraint_force.addGlobalParameter('K', k)
        restraint_force.addPerParticleParameter('x0')
        restraint_force.addPerParticleParameter('y0')
        restraint_force.addPerParticleParameter('z0')
        for idx in indices:
            res_pos = self.positions[idx].value_in_unit_system(md_unit_system)
            restraint_force.addParticle(idx, res_pos)

        self.system.addForce(restraint_force)

    def flat_bottom_repulsive(self, idx1, idx2, r0, k):
        fbr = mm.CustomBondForce(
            'step(r0-r) * (k/2) * (r0-r)^2')
        fbr.addGlobalParameter('r0', r0)
        fbr.addGlobalParameter('k', k)
        self.system.addForce(fbr)
        for i, j in zip(idx1, idx2):
            #fbr.addBond(i, j, [r0, k])
            fbr.addBond(i, j)


    def get_nonsolvent_indices(self, tag='Na+'):
        ''' Assumes prmtop is ordered thusly: solute,ction,solvent and gives
            you all nonsolvent indices and symbols based on the ction symbol '''

        atom_symbols = [a.name for a in self.topology.atoms()]
        last = [idx for idx, s in enumerate(atom_symbols) if s==tag][-1] + 1
        idx = list(range(last))
        syms = [atom_symbols[i] for i in idx]

        return idx, syms


    def add_barostat(self, pressure=1, temperature=300):
        self.system.addForce(MonteCarloBarostat(pressure * u.bar, temperature * u.kelvin))


    def set_integrator(self, ensemble='NVT', temperature=300, fc=1, timestep=2):
        """ Only Langevin so far.
            fc = friction in 1/ps
            temperature in Kelvin
            timestep in fs. """

        # TODO: Add NVE possibilty

        if ensemble == 'NVT':
            # pylint: disable=maybe-no-member
            self.integrator = LangevinIntegrator(temperature * u.kelvin,
                                                 fc / u.picosecond,
                                                 timestep * u.femtoseconds)

        self.timestep = timestep


    def init_simulation(self, box_vectors=None, positions=None, platform=None,
                        double=False, pbcbonds=True):
        ''' When you do this, charges, custom forces and constraints
            CANNOT be changed afterwards. '''

        if not positions:
            positions = self.positions

        # this is off by default, since their proteins are too big to diffuse
        # from image to image, i guess? Maybe that is why things randomly
        # blow up..
        if pbcbonds:
            for force in self.system.getForces():
                if isinstance(force, HarmonicBondForce):
                    force.setUsesPeriodicBoundaryConditions(True)
                elif isinstance(force, HarmonicAngleForce):
                    force.setUsesPeriodicBoundaryConditions(True)
                elif isinstance(force, PeriodicTorsionForce):
                    force.setUsesPeriodicBoundaryConditions(True)


        # TODO: kwargs...
        if platform:
            'OpenCL, CUDA, CPU, or Reference'
            platform = Platform.getPlatformByName(platform)
            if double:
                properties = {'CudaPrecision':'double'}
                self.simulation = app.Simulation(self.topology, self.system,
                                                 self.integrator, platform, properties)
            else:
                self.simulation = app.Simulation(self.topology, self.system,
                                                 self.integrator, platform)
        else:
            self.simulation = app.Simulation(self.topology, self.system, self.integrator)

        self.simulation.context.setPositions(positions)

        # TODO: get boxvectors from prmtop or crd or wherever.f
        if box_vectors:
            self.simulation.context.setPeriodicBoxVectors(*box_vectors)
        elif self.boxvectors:  # loaded from rst7
            print('using rst7/dcd boxvectors')
            self.simulation.context.setPeriodicBoxVectors(*self.boxvectors)

        if self.velocities:  # set velocities to state saved in self
            self.simulation.context.setVelocities(*self.velocities)


    def minimize(self, **kwargs):
        self.simulation.minimizeEnergy(**kwargs)


    def mbd(self, temperature=300):
        ''' Maxwell-Boltzmann distribute velocities. '''
        self.simulation.context.setVelocitiesToTemperature(temperature * u.kelvin)


    def standard_reporters(self, step=500, to_screen=True):
        self.simulation.reporters.append(app.DCDReporter(os.path.join(self.fpath, self.tag) + '.dcd', step))
        olist = [os.path.join(self.fpath, self.tag) + '.log']

        if to_screen:
            olist = [stdout] + olist
        for out in olist:
            self.simulation.reporters.append(app.StateDataReporter(out, step, step=True, speed=True,
                                             potentialEnergy=True, temperature=True, volume=True,
                                             density=True, separator='\t'))

    def add_reporter(self, rep):
        ''' add custom reporter object '''
        self.simulation.reporters.append(rep)

    def run(self, time_in_ns=1, steps=None, time_in_ps=None):
        ''' Somewhat silly. Only fill out one of them ok. '''

        # TODO: Dont be silly. Or add some checks.
        if steps:
            runsteps = steps
        if time_in_ns:
            runsteps = int(time_in_ns * 1e6 / self.timestep)
        if time_in_ps:
            runsteps = int(time_in_ps * 1e3 / self.timestep)

        self.simulation.step(runsteps)
        o = os.path.join(self.fpath, self.tag)
        self.simulation.saveState(o + '.xml')
        restrt = RestartReporter(o +  '.rst7', 10000)
        # pylint: disable=unexpected-keyword-arg
        state = self.simulation.context.getState(getPositions=True, # pylint: disable=no-value-for-parameter
                                                 getVelocities=True,
                                                 enforcePeriodicBox=True)
        restrt.report(self.simulation, state)

